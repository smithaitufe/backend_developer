# The Trip Guru Challenge.

The test will consist of 3 main point

* Research
* Knowledge
* Adaptability


In the repository, you will find an empty Lumen project https://lumen.laravel.com/

The first goal is spin up the service using Docker. You don’t need to fully understand Docker but you need to use it for run the project. The instruction for how to use Docker for this project are below.

Once you finished the Docker's configuration you should be able to see in http://test.local.thetripguru.com:8090/test a message saying “Welcome to The Trip Guru Challenge”.

The second Goal, create an endpoint /fuzz/{number}, the {number} need to be dynamic and you should be able to go to /fuzz/5 or /fuzz/10 and have different results. The result of the endpoint will print the numbers from 1 to {number}. But for multiples of three print “Fizz” instead of the number and for the multiples of five print “Buzz”. For numbers which are multiples of both three and five print “FizzBuzz”.” the response should be in Json. The way you organize the code is up to you.

Lately, you need to write a test to check a battery of expected results. This test needs to be written in PHPUnit https://lumen.laravel.com/docs/5.2/testing would be nice to use Data Providers here.

After you finish the project do a Pull Request to the origin repository. With a proper comment to understand what you did.


## Docker configuration

Before we start you need to have the port 8090 free or change it in the file docker-compose.yml

* Install Docker CE https://docs.docker.com/install/

* Install Docker-compose  https://docs.docker.com/compose/install/

* Go to the project root directory and run docker-compose up -d
This should download all the images for the project you can check if work running

```
$ docker ps
```

You should see something similar to

```
                                  COMMAND                  CREATED             STATUS              PORTS                              NAMES
cf6d9b1b8359        pablofmorales/apache-php7:7.2-apache   "docker-php-entrypoi…"   29 minutes ago      Up 29 minutes       0.0.0.0:8090->80/tcp               tripguru_backend_test
```

* Host file, you need to change your host file to indicate to resolve test.local.thetripguru.com in localhost

```
127.0.0.1 test.local.thetripguru.com
```

